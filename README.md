# Oshipka - Declarative WebApp

PaaS - creates easily deployable declarative web applications with (moslty) file-system dependencies and progressive enhancement philosophy.

### Provides:
* Bootstrap your own project with `oshipka bootstrap <project_name>`
* Generate models, routes and templates using `view_models/*.yaml` declarative approach
* Frontend niceties such chosen.js, datatables.js and lightbox.js - but apps should still work without JS.
* Off-request worker based on a file-system persistence and streaming intermediary results (if js is supported, or upon refresh)
* Whooshalchemy search
* Installation with gunicorn, generates nginx config and letsencrypt certificate
* Based on [Flask](), [Sqlalchemy](), [TWW](https://gitlab.com/pisquared/tww) and others


## TODO
* [ ] [list] pagination of many items
* [ ] [prod_install] Download sensitive prompt
* [ ] [vm_gen] Auto generate dependency graph for `view_models/_process_order`
* [ ] [auto_dns] Start and test `auto_dns.service`
* [ ] [migrations] ALTER doesn't work with sqlite3