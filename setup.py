from setuptools import setup, find_packages


def gen_from_file():
    with open("requirements.txt") as f:
        return [line.strip() for line in f.readlines()]


setup(name='oshipka',
      version='0.3',
      description='oshipka',
      url='http://gitlab.com/pisquared/oshipka',
      author='Daniel Tsvetkov',
      install_requires=gen_from_file(),
      author_email='',
      license='MIT',
      packages=find_packages(),
      )
