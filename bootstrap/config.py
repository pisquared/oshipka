import os

basepath = os.path.dirname(os.path.realpath(__file__))

DATA_DIR = os.path.join(basepath, "data")
STATIC_DATA_DIR = os.path.join(basepath, "data_static")

MEDIA_DIR = os.path.join(DATA_DIR, "media")

TASKS_DIR = os.path.join(DATA_DIR, "tasks")
TASKS_IN_DIR = os.path.join(TASKS_DIR, "in")
TASKS_BUF_DIR = os.path.join(TASKS_DIR, "buf")
TASKS_PROC_DIR = os.path.join(TASKS_DIR, "proc")

DATABASE_FILE = os.path.join(DATA_DIR, "db.sqlite")
SQLALCHEMY_DATABASE_URI = 'sqlite:///{}'.format(DATABASE_FILE)

SEARCH_INDEX_PATH = os.path.join(DATA_DIR, "search_index")

TEMPLATES_FOLDER = os.path.join(basepath, "webapp", "templates")
STATIC_FOLDER = os.path.join(basepath, "webapp", "static")

MAKEDIRS = [
    DATA_DIR, STATIC_DATA_DIR, MEDIA_DIR, TASKS_DIR, TASKS_IN_DIR, TASKS_PROC_DIR, TASKS_BUF_DIR,
]
