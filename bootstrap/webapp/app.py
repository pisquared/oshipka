from flask import render_template

from webapp.routes import *
from oshipka.webapp import app


@app.route('/')
def home():
    return render_template("home.html")
