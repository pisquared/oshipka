from oshipka.persistance import db


def populate_db(app):
    with app.app_context():
        db.session.commit()