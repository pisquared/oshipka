from flask_migrate import MigrateCommand
from flask_script import Manager

from config import SQLALCHEMY_DATABASE_URI
from oshipka.persistance import db, migrate
from oshipka.webapp import app

app.config["SQLALCHEMY_DATABASE_URI"] = SQLALCHEMY_DATABASE_URI
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
db.init_app(app)
migrate.init_app(app, db)
manager = Manager(app)
manager.add_command('db', MigrateCommand)
from webapp.models import *

if __name__ == '__main__':
    manager.run()
