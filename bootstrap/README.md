# Project

## Install

```
virtualenv -p python3 venv
source venv/bin/activate
pip install -r requirements.txt

```

## Run

```
source venv/bin/activate
python run.py
```

_Powered by [oshipka](https://gitlab.com/pisquared/oshipka)_