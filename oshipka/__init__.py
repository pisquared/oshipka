if __name__ == "__main__":
    from oshipka.persistance import init_db
    from oshipka.webapp import app

    init_db(app)

    app.run(debug=True)
