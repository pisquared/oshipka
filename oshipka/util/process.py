import traceback
from time import time, sleep


def process_exp_backoff(func, func_args=None, func_kwargs=None, func_exc_classes=None,
                        max_attempts=3, initial_sleep_time=1, sleep_exp=2, max_sleep_time=0, timeout=0):
    func_args = list() if func_args is None else func_args
    func_kwargs = dict() if func_kwargs is None else func_kwargs
    func_exc_classes = Exception if func_exc_classes is None else func_exc_classes
    attempt, start_time, sleep_time = 1, time(), initial_sleep_time
    while True:
        try:
            print("EXP_BACKOFF: Attempt {}/{}".format(attempt, max_attempts))
            return func(*func_args, **func_kwargs)
        except func_exc_classes as e:
            print("EXP_BACKOFF: Received error at attempt: {}/{}".format(attempt, max_attempts))
            traceback.print_exc()
            print(e)
        total_time = time() - start_time
        if (max_sleep_time and sleep_time >= max_sleep_time) or \
                (max_attempts and attempt >= max_attempts) or \
                (timeout and timeout >= total_time):
            raise Exception("EXP_BACKOFF: Giving up: \n"
                            "- sleep_time: {}/{}\n"
                            "- attempt: {}/{}\n"
                            "- total_time: {}/{}\n".format(
                sleep_time, max_sleep_time,
                attempt, max_attempts,
                total_time, timeout))
        sleep_time *= sleep_exp
        print("EXP_BACKOFF: Sleeping {}...".format(sleep_time))
        sleep(sleep_time)
        attempt += 1


def process_iterable(iterable: set, process_func, processed: set = None, errors: set = None,
                     f_args: list = None, f_kwargs: dict = None):
    processed = set() if not processed else processed
    errors = set() if not errors else errors
    f_args = list() if not f_args else f_args
    f_kwargs = dict() if not f_kwargs else f_kwargs

    to_process = iterable  # - processed - errors
    tot_iterables = len(to_process)
    tot_start = time()

    for i, element in enumerate(to_process):
        print("Processing {}/{} -> {}".format(i + 1, tot_iterables, element))
        start = time()
        try:
            process_func(element, *f_args, **f_kwargs)
            processed.add(element)
        except Exception as e:
            errors.add(element)
            traceback.print_exc()
            print(errors)
            print(processed)
            print(e)

        end = time() - start
        tot_end = time() - tot_start
        avg_per_element = tot_end / (i + 1)
        eta = avg_per_element * (tot_iterables - i + 1)
        eta_min = eta / 60
        print("Processed. Time took: {:.0f}. Tot time: {:.0f}.".format(end, tot_end))
        print("Avg per element: {:.0f}. ETA: {:.0f} ({:.1f} min)".format(avg_per_element, eta, eta_min))
