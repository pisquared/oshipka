import os
import shlex
import subprocess


def run_os_cmd(command):
    output = os.popen(command).read()
    return output


def run_cmd(command):
    process = subprocess.Popen(shlex.split(command),
                               stdout=subprocess.PIPE,
                               stderr=subprocess.STDOUT,
                               universal_newlines=True,
                               )
    for line in iter(process.stdout.readline, ''):
        yield line


def print_cmd(command):
    for line in run_cmd(command):
        print(line, end='')


if __name__ == "__main__":
    print_cmd("env")
