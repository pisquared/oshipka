import re


def camel_case_to_snake_case(name):
    """
    Convertes a CamelCase name to snake_case
    :param name: the name to be converted
    :return:
    """
    s1 = re.sub('(.)([A-Z][a-z]+)', r'\1_\2', name)
    return re.sub('([a-z0-9])([A-Z])', r'\1_\2', s1).lower()


def snake_case_to_camel_case(name):
    """
    Convertes a snake_case name to CamelCase
    :param name: the name to be converted
    :return:
    """
    return ''.join(x.title() for x in name.split('_'))