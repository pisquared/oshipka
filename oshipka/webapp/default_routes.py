from flask import send_from_directory

from oshipka.webapp import oshipka_bp
from config import MEDIA_DIR


@oshipka_bp.route('/media/<path:filepath>')
def get_media(filepath):
    return send_from_directory(MEDIA_DIR, filepath)
