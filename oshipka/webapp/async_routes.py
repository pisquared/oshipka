import json
import os
import subprocess
import time
from uuid import uuid4

from config import TASKS_BUF_DIR
from flask import render_template, request, Response, redirect, url_for, jsonify
from flask_table import Table, LinkCol, Col

from oshipka.persistance import db
from oshipka.webapp import test_bp, oshipka_bp
from oshipka.webapp.views import catch_flash

TASKS = {}


def register_task(task_name, task_func, package, *args, **kwargs):
    TASKS[task_name] = {
        "func": task_func,
        "package": package,
    }


def stateful_task(*args, **kwargs):
    n = 5
    for i in range(0, n + 1):
        print(i)
        time.sleep(1)


register_task("stateful_task", stateful_task, "oshipka.webapp.async_routes.{}")


class Task(db.Model):
    id = db.Column(db.Integer, primary_key=True)

    name = db.Column(db.Unicode)
    uuid = db.Column(db.Unicode)
    status = db.Column(db.Unicode, default="NOT_STARTED")

    func_name = db.Column(db.Unicode)
    args = db.Column(db.Unicode, default="[]")
    kwargs = db.Column(db.Unicode, default="{}")

    def serialize(self):
        return dict(
            name=self.name, uuid=self.uuid, kwargs=json.loads(self.kwargs),
            status=self.status,
        )


class TasksTable(Table):
    uuid = LinkCol('Task', "test_bp.get_task_status", url_kwargs=dict(task_uuid='uuid'))
    name = Col('name')


@test_bp.route("/tasks", methods=["GET"])
def list_tasks():
    tasks = Task.query.all()
    tasks_table = TasksTable(tasks)
    return render_template("test/tasks.html",
                           runnable_tasks=TASKS.keys(),
                           tasks_table=tasks_table)


def tail(filename):
    process = subprocess.Popen(['tail', '-F', filename],
                               stdout=subprocess.PIPE,
                               stderr=subprocess.STDOUT,
                               universal_newlines=True,
                               )
    for line in iter(process.stdout.readline, b''):
        line = line.strip()
        yield "data: {}\n\n".format(line)


def worker_start_task(task_name, task_kwargs):
    reg_task = TASKS.get(task_name)
    func_name = reg_task.get('package').format(reg_task.get('func').__name__)
    uuid = str(uuid4())
    task = Task(name=task_name,
                uuid=uuid,
                kwargs=json.dumps(task_kwargs),
                func_name=func_name
                )
    db.session.add(task)
    db.session.commit()
    return uuid


@oshipka_bp.route('/tasks/<task_name>/start', methods=['POST'])
def start_task(task_name):
    task_kwargs = {k: v for k, v in request.form.items() if k != 'csrf_token'}
    task_uuid = worker_start_task(task_name, task_kwargs)
    return task_uuid


@test_bp.route('/tasks/<task_name>/start', methods=['GET', 'POST'])
def start_task(task_name):
    task_kwargs = {k: v for k, v in request.form.items() if k != 'csrf_token'}
    task_uuid = worker_start_task(task_name, task_kwargs)
    return redirect(url_for('test_bp.get_task_status', task_uuid=task_uuid))


def get_task_ctx(task_uuid):
    ctx = {}
    task = Task.query.filter_by(uuid=task_uuid).first()
    ctx['task'] = task.serialize()
    out_filename = os.path.join(TASKS_BUF_DIR, task_uuid)
    if os.path.exists(out_filename):
        with open(out_filename) as f:
            ctx['stdout'] = f.read()
    return ctx


@test_bp.route('/task_status/<task_uuid>')
def get_task_status(task_uuid):
    ctx = get_task_ctx(task_uuid)
    return render_template("test/task_status.html", **ctx)


@oshipka_bp.route('/stream/<task_uuid>')
def stream(task_uuid):
    if request.headers.get('accept') == 'text/event-stream':
        task = Task.query.filter_by(uuid=task_uuid).first()
        if not task:
            return jsonify({"error": "no task with uuid {}".format(task_uuid)}), 404
        return Response(tail(os.path.join(TASKS_BUF_DIR, task_uuid)), content_type='text/event-stream')
    return jsonify({"error": "Request has to contain 'Accept: text/event-stream' header"}), 400


@test_bp.route("/div_error")
@catch_flash
def test_error():
    return 1 / 0
