function start_async_task(task_name, data, ondata) {
    $.ajax({
        url: "/tasks/" + task_name + "/start",
        method: "post",
        data: data,
        success: function (r) {
            let source = new EventSource('/stream/' + r);
            source.onmessage = function (e) {
                let state = e.data;
                ondata(e.data)

            }
        }
    })
}

function createSearch(options) {
    /**
     * options:
     * - searchElSel - element (usually input) that will be used to take query from
     * - filterElSel - elements that will be shown/hidden depending on query match
     * - filterElDataName - [default: searchable-content] the name of the data element that will contain the search document
     * - resultsElSel - [default: false] results element to display instead of in-place
     * - resultsElTitle - [default: <h3>Results</h3>] - title to show if there are results and there is resultsElSel
     * - fuseOptions - options to pass to fuse search
     * @type {*|string}
     */
    let searchElSel = options.searchElSel || "#input-search";
    let filterElSel = options.filterElSel || "#elements-to-filter-list";
    let resultsElSel = options.resultsElSel || false;
    let resultsElTitle = options.resultsElSel || "<h3>Results</h3>";
    let filterElDataName = options.filterElDataName || "searchable-content";
    let fuseOptions = options.fuseOptions || {
        includeScore: true,
    }

    let inputEl = $(searchElSel);
    let elements = $(filterElSel);
    let resultsEl = $(resultsElSel)
    let documents = $.map(elements, function (el) {
        return $(el).data(filterElDataName)
    });
    const fuse = new Fuse(documents, fuseOptions)

    return {
        search: function () {
            let query = inputEl.val();
            if (query === "") {
                return;
            }
            let results = fuse.search(query);
            let showElIds = $.map(results, function (r) {
                return r.refIndex
            });
            if (resultsEl) {
                $(resultsEl).html(resultsElTitle);
                $(elements).hide();
            } else {
                $(resultsEl).hide();
                $.each($(elements), function (el) {
                    $(el).show();
                });
            }
            $.each(elements, function (elId, el) {
                if (showElIds.includes(elId)) {
                    $(el).show();
                    if (resultsEl) {
                        $(resultsEl).append($(el));
                    }
                } else {
                    $(el).hide();
                }
            })
        }
    }
}