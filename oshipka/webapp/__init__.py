import os

from flask import Flask, Blueprint

OSHIPKA_PATH = os.getenv('OSHIPKA_PATH', '/home/pi2/workspace/oshipka')

app = Flask(__name__)
app.config["SECRET_KEY"] = "UNSECRET_KEY....478932fjkdsl"
app.config["SECURITY_PASSWORD_SALT"] = "UNSECRETfdsafsda_KEY....478932fjkdsl"

test_bp = Blueprint('test_bp', __name__,
                    url_prefix="/test",
                    template_folder='templates',
                    static_folder='static',
                    )

oshipka_bp = Blueprint('oshipka_bp', __name__,
                       template_folder='templates',
                       static_url_path='/oshipka/static',
                       static_folder=os.path.join(OSHIPKA_PATH, 'oshipka', 'webapp', 'static'),
                       )

import oshipka.webapp.async_routes
import oshipka.webapp.default_routes
