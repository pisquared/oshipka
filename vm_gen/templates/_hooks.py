from oshipka.webapp.views import ViewContext, default_get_args_func, default_get_func, default_list_func, \
    default_get_form_func, default_create_func, default_update_func, default_delete_func, default_search_func


def get_template(vc):
    vc.template = "{}/get.html".format(vc.model_view.model_name)


def list_template(vc):
    vc.template = "{}/list.html".format(vc.model_view.model_name)


def table_template(vc):
    vc.template = "{}/table.html".format(vc.model_view.model_name)


def search_template(vc):
    vc.template = "{}/search.html".format(vc.model_view.model_name)


def create_template(vc):
    vc.template = "{}/create.html".format(vc.model_view.model_name)


def update_template(vc):
    vc.template = "{}/update.html".format(vc.model_view.model_name)


def delete_template(vc):
    vc.template = "delete_instance.html".format(vc.model_view.model_name)


get_view_context = ViewContext(
    filter_func=default_get_func,
    template_func=get_template,
)

list_view_context = ViewContext(
    filter_func=default_list_func,
    template_func=list_template,
)

table_view_context = ViewContext(
    filter_func=default_list_func,
    template_func=table_template,
)

search_view_context = ViewContext(
    filter_func=default_search_func,
    template_func=list_template,
)

create_view_context = ViewContext(
    args_get_func=default_get_form_func,
    template_func=create_template,
    execute_func=default_create_func,
)

update_view_context = ViewContext(
    args_get_func=default_get_form_func,
    filter_func=default_get_func,
    template_func=update_template,
    execute_func=default_update_func,
)

delete_view_context = ViewContext(
    args_get_func=default_get_form_func,
    filter_func=default_get_func,
    template_func=delete_template,
    execute_func=default_delete_func,
)
