import os


def order_from_process_order(file_ext, directory):
    model_names = set([f.split('.{}'.format(file_ext))[0] for f in os.listdir(directory) if f.endswith(".{}".format(file_ext))])
    process_order_file = os.path.join(directory, "_process_order")
    ordered_model_names = []
    # process first ordered if exists
    if os.path.exists(process_order_file):
        with open(process_order_file) as f:
            for line in f.readlines():
                line = line.strip()
                if line:
                    ordered_model_names.append(line)
                    model_names.remove(line)
    for model_name in model_names:
        ordered_model_names.append(model_name)
    return ordered_model_names