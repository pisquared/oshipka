import json

from flask import Flask, render_template, request, jsonify

app = Flask(__name__)


@app.route('/')
def home():
    return render_template("bootstrap.html")


@app.route('/config', methods=["post"])
def post_config():
    config = json.loads(request.form)

    return jsonify(
        {
            "raw": config,
        })


if __name__ == "__main__":
    app.run(debug=True, port=4999)
