import re

from oshipka.util.os import run_os_cmd

PORT = 5000

ipv4_regex = re.compile('^(?:[0-9]{1,3}\.){3}[0-9]{1,3}$')


def find_port():
    for i in range(16):
        port = PORT + i
        has_port = run_os_cmd("ss -tl | grep {}".format(port))
        if not has_port:
            return port


def find_private_ipv4():
    private_ips = run_os_cmd("hostname -I").split()
    for private_ip in private_ips:
        private_ip = private_ip.strip()
        if ipv4_regex.match(private_ip):
            return private_ip


if __name__ == "__main__":
    print(find_port())
