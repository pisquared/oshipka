import os
import shutil

from jinja2 import FileSystemLoader, Environment

from oshipka.util.os import run_cmd
from provision.util import find_port, find_private_ipv4

USER = "pi2"
PARENT_DOMAIN = "pi2.dev"
MAX_UPLOAD_SIZE = "10m"


oshipka_path = os.environ.get('OSHIPKA_PATH')
TEMPLATES_PATH = os.path.join(oshipka_path, "provision", "templates")
TMP_PATH = os.path.join(oshipka_path, "provision", "tmp")
os.makedirs(TMP_PATH, exist_ok=True)
shutil.rmtree(TMP_PATH)
os.makedirs(TMP_PATH, exist_ok=True)
env = Environment(
    loader=FileSystemLoader(searchpath=TEMPLATES_PATH),
)


def prod_install():
    pwd = next(run_cmd('pwd')).strip()

    project_name = os.path.basename(pwd)
    project_domain = "{}.{}".format(project_name, PARENT_DOMAIN)
    port = find_port()
    upstream_ip = find_private_ipv4()

    ctx = dict(
        user=USER,
        pwd=pwd,
        oshipka_path=oshipka_path,
        project_name=project_name,
        project_domain=project_domain,
        upstream_ip=upstream_ip,
        max_upload_size=MAX_UPLOAD_SIZE,
        port=port,
    )

    tmpl_fname = [
        ('gunicorn.service', "{}.service".format(project_name)),
        ('worker.service', "{}_worker.service".format(project_name)),
        ('nginx_insecure.conf', "{}.insecure".format(project_domain)),
        ('nginx.conf', "{}.conf".format(project_domain)),
    ]

    for tmpl, fname in tmpl_fname:
        actual_tmpl = env.get_template(tmpl)
        rendered_tmpl = actual_tmpl.render(ctx)
        with open(os.path.join(TMP_PATH, fname), 'w') as f:
            f.write(rendered_tmpl)


if __name__ == "__main__":
    prod_install()
