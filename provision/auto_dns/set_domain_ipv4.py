import random
import sys
from time import sleep

import requests

from oshipka.util.os import run_os_cmd
from oshipka.util.process import process_exp_backoff
from provision.auto_dns.check import ipv4_sites, set_resolver_results, DEFAULT_TTL


def verify_dns_domain(domain, ipv4):
    ip_on_dns = run_os_cmd("dig +short {}".format(domain))
    if not ip_on_dns:
        raise Exception("Record for {} doesn't exist".format(domain))
    ip_on_dns = ip_on_dns.strip()
    assert ip_on_dns == ipv4


def set_domain_ipv4(domain):
    url = random.choice(ipv4_sites)
    resp = requests.get(url)
    ipv4 = resp.text.strip()
    print('Got IP: {}'.format(ipv4))
    try:
        verify_dns_domain(domain, ipv4)
        print("DNS already set up for {}".format(domain))
    except Exception:
        process_exp_backoff(set_resolver_results, func_args=[domain, 'A', ipv4])
        print("Sleeping 5 sec...")
        sleep(5)
        print("Checking that {} is set".format(domain))
        process_exp_backoff(verify_dns_domain, func_args=[domain, ipv4], max_attempts=0, max_sleep_time=DEFAULT_TTL)


if __name__ == "__main__":
    domain = sys.argv[1]
    if not domain:
        print('domain is required first argument')
    set_domain_ipv4(domain)
